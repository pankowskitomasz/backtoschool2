import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import SuppliesS1 from "../components/supplies-s1";
import SuppliesS2 from "../components/supplies-s2";
import SuppliesS3 from "../components/supplies-s3";
import SuppliesS4 from "../components/supplies-s4";

class Supplies extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <SuppliesS1/>
                <SuppliesS2/>
                <SuppliesS3/>
                <SuppliesS4/>
            </Container>    
        );
    }
}

export default Supplies;